using Gpseq;
using Gpseq.Collectors;
using Gee;

void main () {
    //  Where_Simple1();
    //  Where_Simple2();
    Aggregation.aggregateSimple();
    Aggregation.aggregateSeed();
    Aggregation.Average();
    Aggregation.Count();
    //  Aggregation.LongCount();
    Aggregation.Max();
    Aggregation.Min();
    Aggregation.Sum();
    Conversion.AsEnumerable();
    Conversion.Cast();
    Conversion.ToArray();
    Conversion.ToDictionary();
    Conversion.ToList();
    Conversion.ToLookup();
    
    Element.ElementAt();
    //  Element.ElementAtOrDefault();
    Element.Firstsimple();
    Element.Firstconditional();
    Element.Firstsimple();
    Element.ThrowErrorForSomeReason();
    Element.Last();
    GroupBy.GroupBy();
    Ordering.OrderByNumbers();
    Ordering.OrderByDates();
    Ordering.OrderByObject();
    Ordering.OrderByDescending();
    Ordering.Reverse();
}

namespace Aggregation{
    void aggregateSimple (){
        var numbers = new int?[] { 1, 2, 3, 4, 5 };
        var result = Seq.of_array(numbers)
            .reduce((a, b) => a * b).value.value;
        prin("Aggregated numbers by multiplication:\n",result);
    }
    void aggregateSeed (){
        var numbers = new int?[] { 1, 2, 3 };
        var result = Seq.of_array(numbers)
            .reduce((a, b) => a + b).value.value + 10;// ???
        prin("Aggregated numbers by multiplication:\n",result);
    }
    void Average (){
        //  var result = (double)Seq.of_array(numbers)
        //      .reduce((a, b) => a + b).value.value / numbers.length;
        int?[] numbers = { 10, 10, 11, 11 };
        var result = Seq.of_array(numbers).collect(average_double<int?>(g => (double)g)).value;
        prin("Average is: \n", result);
    }
    void Count (){
        string[] names = { "Peter", "John", "Kathlyn", "Allen", "Tim" };
        var result = Seq.of_array(names).count().value;
        prin("Counting names gives: \n", result);
    }
    void LongCount (){
        var result = Seq.iterate<int32>(0, i => i < int32.MAX, i => ++i).parallel().count().value;
        //  var result = Seq.iterate( 0, i => i < int32.MAX, i => i++).count().value;
        prin("Counting names gives: \n", result);
    }
    void Max (){
        int?[] numbers = { 2, 8, 5, 6, 1 };
        var result = Seq.of_array(numbers).max((a, b) => {
            return a < b ? -1 : (a == b ? 0 : 1);
        }).value.value;
        prin("Gpseq highest number is: \n", result);
    }
    void Min (){
        int?[] numbers = { 6, 9, 3, 7, 5 };
        var result = Seq.of_array(numbers).min((a, b) => {
            return a < b ? -1 : (a == b ? 0 : 1);
        }).value.value;
        prin("Gpseq lowest number is: \n", result);
    }
    void Sum (){
        int?[] numbers = { 2, 5, 10 };
        var result = Seq.of_array(numbers)
            .reduce((a, b) => a + b).value.value;
        prin("Summing the numbers yields: \n", result);
    }
}

namespace Conversion{
    void AsEnumerable(){
        string[] names = { "John", "Suzy", "Dave" };
        prin("Iterating over IEnumerable collection:");
        foreach (var name in names)
            prin(name);
    }
    void Cast(){
        var vegetables = new ArrayList<string>.wrap({ "Cucumber", "Tomato", "Broccoli" });
        var result = vegetables.to_array();
        prin("List of vegetables casted to a simple string array:");
        foreach(var vegetable in result)
            prin(vegetable);
    }
    //  void OfType(){
    //      var objects = 
    //  }
    void ToArray(){
        int[] numbers = { 4, 2, 3, 4 };
        var result = numbers;
        result[0] = 1; assert(result[0] != numbers[0]);//Its a copy!
        prin("New array contains identical values:");
        foreach(var number in result)
            prin(number);
    }

    class English2German{
        public string EnglishSalute { get; set; }
        public string GermanSalute { get; set; }
    }
    void ToDictionary(){
        English2German[] english2German = { 
        new English2German() { EnglishSalute = "Good morning", GermanSalute = "Guten Morgen" },
        new English2German() { EnglishSalute = "Good day", GermanSalute = "Guten Tag" },
        new English2German() { EnglishSalute = "Good evening", GermanSalute = "Guten Abend" },
    };
    var result = Seq.of_array(english2German)
        .collect(to_map<string,string,English2German>(
            k => k.EnglishSalute,
            g => g.GermanSalute, (a,b)=> a)).value;
        foreach (var item in result.entries) {
            prin("English salute ",item.key, " is ",item.value, " in German");
        }
    }

    void ToList(){
        var names = new ArrayList<string>.wrap({"Brenda", "Carl", "Finn"});
        var result = Seq.of_collection<string>(names)
                           .collect( to_list<string>() )
                           .value;
        prin("names is of type: ", names.get_type().name());
        prin("result is of type: ",result.get_type().name());
        prin("IDK, there no just list in Gee");
    }
    void ToLookup(){
        string[] words = {"one", "two", "three", "four", "five", "six", "seven"};
        Map<int,Gee.List<string>> result = Seq.of_array<string>(words)
                                            .group_by<int>(g => g.length)
                                            .value;
        for (int i = 0; i < 5; i++) {
            prin(@"Elements with a length of $i:");
            foreach (var word in result[i]) 
                prin(word);
        }
        prin("Here u can see the Vala nullability property");
        prin("If the collection was a string? u wouldn't have seen the warnings ^^");
    }
}

namespace Element { 
    void ElementAt(){
        string[] words = { "One", "Two", "Three" };
        var result = words[1];// ???
        prin("If you implement a method T2 get(T1 index) in a class then the [] operator will implicitly call it");
        prin("Element at index 1 in the array is:\n", result);
    }
    void ElementAtOrDefault(){
        string[] colors = { "Red", "Green", "Blue" };
        var resultIndex1 = colors[1];
        //  var resultIndex10 = colors[10] ?? string.default???;
        prin("Element at index 1 in the array contains:\n", resultIndex1);
        //  prin("Element at index 10 in the array does not exist:\n", resultIndex10);
    }
    void Firstsimple(){
        var fruits = new ArrayList<string>.wrap({"Banana", "Apple", "Orange"});
        var result = fruits.first();
        prin("First element in the array is:\n",result);

    }
    void Firstconditional(){
        string[] countries = {"Denmark", "Sweden", "Norway"};
        var result = Seq.of_array<string>(countries)
            .find_first(g => g.length == 6).value.value;
        prin("First element in the array is:\n",result);
    }
    void Last(){
        var fruits = new ArrayList<string>.wrap({"Banana", "Apple", "Orange"});
        var result = fruits.last();
        prin("First element in the array is:\n",result);
        assert(result == "Orange");
    }
    void LastOrDefaultsimple(){

    }
    void LastOrDefaultconditional(){
        prin("There no find last:\n");
    }
    void ThrowErrorForSomeReason(){
        var names1 = new ArrayList<string>.wrap({"Peter"});
        var names3 = new ArrayList<string>.wrap({"Peter", "Joe", "Wilma"});
        var empty = new ArrayList<string>();
        //  try {
            //  }catch (Error e) {
                //          error(e.message);
                //  }
        var result1 = names1.first();
        var result3 = names3.first();
        prin("Here assertion will fail, so commented, but u can do like this");
        //  var resultEmpty = empty.first();
        try {
            // Error type here is randomly picked, Im too lazy to create my own empty list error domain
            if (empty.is_empty) throw new OptionError.BAD_VALUE("No values in list!");
        } catch (Error e) {
            warning(e.message);
        } 
    }
}

namespace GroupBy { 
    void GroupBy(){
        int?[] numbers = { 10, 15, 20, 25, 30, 35 };
        var result = Seq.of_array(numbers)
            .partition(n => (n % 10 == 0))
            .value;
        foreach (var group in result.entries) {
            if (group.key == true)
                prin("Divisible by 10");
            else
                prin("Not Divisible by 10");

            foreach (int number in group.value)
                prin(number);
        }
    }
}

namespace Join {
    class Language {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    class Person {
        public int LanguageId { get; set; }
        public string FirstName { get; set; }
    }
    void GroupJoin () {
        Language[] languages = new Language[]{
            new Language() { Id = 1, Name = "English" },
            new Language() { Id = 2, Name = "Russian" }
        };

        Person[] persons = new Person[]{
            new Person() { LanguageId = 1, FirstName = "Tom" },
            new Person() { LanguageId = 1, FirstName = "Sandy" },
            new Person() { LanguageId = 2, FirstName = "Vladimir" },
            new Person() { LanguageId = 2, FirstName = "Mikhail" },
        };
        //get map 
    }
}

namespace Ordering { 
    void OrderByNumbers(){
        prin("Ordered list of numbers:");
        var arr = new GenericArray<int>();
	    for (int i = 9; i > 6; i--) arr.add(i);
        var result = Seq.of_array<int>(arr.data)
            .order_by(Functions.get_compare_func_for(typeof(int)))
            .foreach(g => prin(g));
    }
    void OrderByDates(){
        var dates = new DateTime[] {
            new DateTime.utc(2015, 2, 15, 0, 0, 0),
            new DateTime.utc(2015, 3, 25, 0, 0, 0),
            new DateTime.utc(2015, 1, 5, 0, 0, 0)
        };
        prin("Ordered list of numbers:");
        var result = Seq.of_array(dates)
            .order_by((a, b) => a.compare(b))
            .foreach(g => prin(g));
    }

    class Car{
        public string Name { get; set; }
        public int HorsePower { get; set; }
    }
    void OrderByObject(){
        Car[] cars = {
            new Car() { Name = "Super Car",   HorsePower = 215 },
            new Car() { Name = "Economy Car", HorsePower = 75 },
            new Car() { Name = "Family Car",  HorsePower = 145 },
        };
        prin("Ordered list of cars by horsepower:");
        var result = Seq.of_array(cars)
            .order_by(Functions.get_compare_func_for(typeof(int)))
            .foreach(g => prin(g.Name, ": ",g.HorsePower," horses"));
    }
    void OrderByDescending(){
        string[] names = { "Ned", "Ben", "Susan" };
        prin("Descending ordered list of names:");
        Seq.of_array<string>(names)
            .order_by()
            .foreach(g => prin(g));
    }
    void Reverse(){
        char[] characters = { 's', 'a', 'm', 'p', 'l', 'e' };
        var list = new GLib.List<char> ();
        foreach (var item in characters) 
            list.append(item);
        list.reverse();
        prin("Characters in reverse order:");
        foreach (var item in list) 
            prin(item);
    }
    //  void OrderBy(){}
    //  void OrderBy(){}
}


//  void Where_Simple1() {
//      prin(Log.METHOD);
//      int?[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 }; 
//      Seq.of_array<int?>(numbers)
//          .filter(g => g < 5)
//          .foreach(g => prin(g));
//  }

//  class Product{ public string productName; public int unitsInStock; public int unitsPrice;}

//  ArrayList<Product> getProductList(){
//      return new ArrayList<Product>.wrap({
//          new Product(){productName = "Chef Anton's Gumbo Mix", unitsInStock = 0},
//          new Product(){productName = "Alice Mutton",           unitsInStock = 0},
//          new Product(){productName = "Thüringer Rostbratwurst",unitsInStock = 0},
//          new Product(){productName = "Gorgonzola Telino",      unitsInStock = 0},
//          new Product(){productName = "Perth Pasties",          unitsInStock = 0},
//          new Product(){productName = "Vala Porwer!",           unitsInStock = 2020},}); 
//  }

//  void Where_Simple2() {
//      prin(Log.METHOD);
//      var products = getProductList();
//      Seq.of_collection(products)
//          .filter(p => p.unitsInStock == 0)
//          .foreach(p => prin(p.productName, " is sold out!"));
//  }




//  void Where_Simple3() {
//      prin(Log.METHOD," Its to simular to 2, so im lazy for this ^^");
//  }


[Print]
void prin(string s){
    print(s + "\n");
}
